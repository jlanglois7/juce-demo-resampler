#ifndef NLTEST_COMPONENT_H
#define NLTEST_COMPONENT_H

#include "JuceHeader.h"

/**
* @file TestComponent.h
* @copyright Noteloop Systems Inc.
*
* See "Main.cpp" for more details about how to use this class.
* @see NOTELOOP_USE_TEST_WINDOW, Main.cpp
*/
class TestComponent : public juce::Component,
                      public juce::Button::Listener,
                      public juce::Slider::Listener
{
public:
    /**
    * Constructor
    */
    TestComponent();

    /**
    * Destructor
    */
    ~TestComponent();

    //==============================================================================
    //Add custom methods below this line:

    //==============================================================================
    /** @internal */
    void paint (juce::Graphics& g);
    /** @internal */
    void resized();
    /** @internal */
    void buttonClicked (juce::Button* button);
    /** @internal */
    void sliderValueChanged (juce::Slider* slider);

private:
    //==============================================================================
    //Add member objects you would like to display and test below this line:
    juce::ScopedPointer<juce::AudioFormatReaderSource> readerSource;
    juce::ScopedPointer<juce::AudioTransportSource> transport;
    juce::ScopedPointer<juce::ResamplingAudioSource> resampler;

    juce::AudioSourcePlayer player;
    juce::AudioDeviceManager device;

    juce::Slider shifter;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TestComponent);
};

#endif //NLTEST_COMPONENT_H