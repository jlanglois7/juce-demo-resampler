#include "TestComponent.h"

TestComponent::TestComponent()
{
    const double min = 0.001;
    const double max = 2.0;

    shifter.setRange (min, max);
    shifter.addListener (this);
    addAndMakeVisible (&shifter);

    juce::AudioFormatManager formatManager;
    formatManager.registerBasicFormats();

    juce::FileChooser chooser ("Please select an audio file to play, loop and resample in real-time",
                               juce::File::getSpecialLocation (juce::File::userMusicDirectory),
                               formatManager.getWildcardForAllFormats(),
                               true);

    if (! chooser.browseForFileToOpen())
    {
        juce::JUCEApplication::getInstance()->quit();
        return;
    }

    juce::AudioFormatReader* reader = formatManager.createReaderFor (chooser.getResult());
    readerSource = new juce::AudioFormatReaderSource (reader, true);

    transport = new juce::AudioTransportSource();
    transport->setSource (readerSource);

    resampler = new juce::ResamplingAudioSource (transport, false);

    player.setSource (resampler);

    device.initialise (0, 2, nullptr, true);
    device.addAudioCallback (&player);

    juce::AudioDeviceManager::AudioDeviceSetup setup;
    device.getAudioDeviceSetup (setup);

    setup.bufferSize = 480;
    device.setAudioDeviceSetup (setup, true);
    device.getAudioDeviceSetup (setup);

    const double srcRate    = reader->sampleRate;
    const double destRate   = setup.sampleRate;
    const double ratio      = srcRate / destRate;

    resampler->setResamplingRatio (ratio);
    shifter.setValue (resampler->getResamplingRatio(), juce::dontSendNotification);

    readerSource->setLooping (true);
    transport->setLooping (true);
    transport->start();
}

TestComponent::~TestComponent()
{
    if (transport != nullptr)
    {
        transport->stop();
    }

    device.removeAudioCallback (&player);
    player.setSource (nullptr);

    resampler       = nullptr;
    transport       = nullptr;
    readerSource    = nullptr;
}

//==============================================================================
void TestComponent::paint (juce::Graphics& g)
{
    g.fillAll (juce::Colours::darkgrey);

    g.setColour (juce::Colours::white);
}

void TestComponent::resized()
{
    const int sliderHeight = 30;

    shifter.setBounds (sliderHeight,
                       (getHeight() / 2) - (sliderHeight / 2),
                       getWidth() - (sliderHeight * 2),
                       sliderHeight);
}

//==============================================================================
void TestComponent::buttonClicked (juce::Button* /*button*/)
{
}

void TestComponent::sliderValueChanged (juce::Slider* slider)
{
    if (&shifter == slider)
    {
        resampler->setResamplingRatio (shifter.getValue());
    }
}