#include "TestWindow.h"

class JUCEDemoResampler : public juce::JUCEApplication
{
public:
    JUCEDemoResampler() :
        window (nullptr)
    {
    }

    ~JUCEDemoResampler()
    {
    }

    //==============================================================================
    void initialise (const juce::String& /*commandLineParameters*/)
    {
        window = new TestWindow();
    }

    void shutdown()
    {
        window = nullptr;
    }

    //==============================================================================
    void systemRequestedQuit()
    {
        quit();
    }

    //==============================================================================
    const juce::String getApplicationName()
    {
        return ProjectInfo::projectName;
    }

    const juce::String getApplicationVersion()
    {
        return ProjectInfo::versionString;
    }

    bool moreThanOneInstanceAllowed()
    {
        return true;
    }

    void anotherInstanceStarted (const juce::String& /*commandLineParameters*/)
    {
    }

private:
    //==============================================================================
    juce::ScopedPointer<TestWindow> window;
    juce::TooltipWindow tooltipWindow;
};

//==============================================================================
// This macro generates the main() routine that starts the app.
START_JUCE_APPLICATION (JUCEDemoResampler)