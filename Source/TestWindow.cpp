#include "TestWindow.h"

TestWindow::TestWindow() :
    juce::DocumentWindow (juce::JUCEApplication::getInstance()->getApplicationName(),
                          juce::Colours::white,
                          juce::DocumentWindow::closeButton)
{
    //Setup the display style:
    setUsingNativeTitleBar (true);

    //Setup the window:
    setResizable (true, false);
    setSize (400, 250);
    setCentreRelative (0.5f, 0.5f);
    setVisible (true);

    //Setup the TestComponent:
    testComponent = new TestComponent();
    testComponent->setBounds (getBounds());

    setContentOwned (testComponent, false);
}

TestWindow::~TestWindow()
{
    //Shutdown the TestComponent:
    testComponent = nullptr;
}

//==============================================================================
void TestWindow::closeButtonPressed()
{
    juce::JUCEApplication::getInstance()->systemRequestedQuit();
}