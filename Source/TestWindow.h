#ifndef NLTEST_WINDOW_H
#define NLTEST_WINDOW_H

#include "TestComponent.h"

/**
* @file TestComponent.h
* @copyright Noteloop Systems Inc.
*
* See "Main.cpp" for more details about how to use this class.
* @see NOTELOOP_USE_TEST_WINDOW, Main.cpp
*/
class TestWindow : public juce::DocumentWindow
{
public:
    /**
    * Constructor
    */
    TestWindow();

    /**
    * Destructor
    */
    ~TestWindow();

    //==============================================================================
    /** @internal */
    void closeButtonPressed();

private:
    //==============================================================================
    juce::ScopedPointer<TestComponent> testComponent;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TestWindow);
};


#endif //NLTEST_WINDOW_H